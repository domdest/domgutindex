#!/bin/bash
sudo apt-get remove -y \
    docker \
    docker-engine \
    docker.io \
    containerd runc
sudo apt-get update
sudo apt install -y \
    docker.io \
    make \
    rsync
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /var/run/docker.sock
sudo chmod +x /usr/local/bin/docker-compose
/usr/sbin/useradd gitlab-runner
echo -e "BmGWdMx6MRgdPm2R\nBmGWdMx6MRgdPm2R" | passwd gitlab-runner
sudo chown gitlab-runner:docker /var/run/docker.sock