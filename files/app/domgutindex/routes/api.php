<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1/books')->group(function() {
    Route::get('', [App\Http\Controllers\Api\v1\BookController::class, 'index']);
    Route::get('{book}', [App\Http\Controllers\Api\v1\BookController::class, 'show']);
});

Route::prefix('v1/authors')->group(function() {
    Route::get('', [App\Http\Controllers\Api\v1\AuthorController::class, 'index']);
});
