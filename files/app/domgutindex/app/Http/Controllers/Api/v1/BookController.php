<?php

namespace App\Http\Controllers\Api\v1;

use App\Book;
use App\Http\Resources\BookResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function index(Request $request)
    {
        $request->validate([
            'title' => ['string', 'nullable'],
            'author' => ['string', 'nullable']
        ]);

        $title = $request->title;
        $author = $request->author;

        $bookCollection = Book::when($title, function ($query, $title) {
            return $query->where('title', 'LIKE', "%{$title}%");
        })->when($author, function ($query, $author) {
            return $query->whereHas('authors', function ($query) use ($author) {
                $query->where('name', 'LIKE', "%{$author}%");
            });
        })->get();

        return BookResource::collection($bookCollection);
    }

    public function show(Book $book)
    {
        return new BookResource($book);
    }
}
