<?php

namespace App\Http\Controllers\Api\v1;

use App\Author;
use App\Http\Resources\AuthorResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    public function index(Request $request)
    {
        $request->validate([
            'author' => ['string', 'nullable']
        ]);

        $author = $request->author;

        $authorCollection = Author::when($author, function ($query, $author) {
            return $query->where('name', 'like', "%{$author}%");
        })->get();

        return AuthorResource::collection($authorCollection);
    }
}
