<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $authors = StandardResource::collection($this->authors);

        return [
            'id' => $this->id,
            'title' => $this->title,
            'tags' => $this->tags,
            'authors' => $authors,
        ];
    }
}
