<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    public static function boot()
    {
        parent::boot();

        static::creating(function($model) {
            $model->id = $model->id;
        });
    }

    public function authors()
    {
        return $this->belongsToMany(Author::class);
    }
}
