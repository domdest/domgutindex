# API Documentation

The purpose of this document is to track endpoints in the Domgutindex API.

## Authors
[Search for authors](authors/index.md)

## Books
[Show single book](books/show.md)
[Search for books](books/index.md)