# Search books

The purpose of this page is to document the `books/index.http` endpoint, which allows the user
to search for books by partial or full title match.

## Headers
|||
|---|---|
|Name |Value |
|Accepts|application/json|
|Content-Type|application/json|

## Query Parameters
|||
|---|---|
|Name|Type|
|author|string|
|title|string|