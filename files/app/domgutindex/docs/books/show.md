# Show one book

The purpose of this page is to document the `books/show.http` endpoint, which allows the user
to search for authors by a partial or full name match.

## URL
`/api/v1/books/{book}`

## URL Parameters
|||
|---|---|
|Name|Type|
|book|int|

## Headers
|||
|---|---|
|Name |Value |
|Accepts|application/json|
|Content-Type|application/json|
