# Search authors

The purpose of this page is to document the `authors/index.http` endpoint, which allows the user
to search for authors by a partial or full name match.

## Headers
|||
|---|---|
|Name |Value |
|Accepts|application/json|
|Content-Type|application/json|

## Query Parameters
|||
|---|---|
|Name|Type|
|author|string|