<?php

use App\Author;
use App\Book;
use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $bookRows = [];
        $bookData = [];

        $gutIndex = file(config('gutindex_file.path'));

        $bookTableBegin = false;

        $count = 0;

        foreach($gutIndex as $line) {
            if(!isset($bookRows[$count])) {
                $bookRows[$count] = '';
            }

            //Are we sure we're reading the book list?
            if(preg_match("(\s\s[0-9]{4})\n", $line)) {
                $bookTableBegin = true;
            }

            if(!$bookTableBegin) {
                continue;
            }

            //Skip any blank lines
            if (strlen($line) == 2) {
                $count++;
                continue;
            }

            $bookRows[$count] .= $line;
        }

        foreach($bookRows as $key => $row) {
            $bookData['title'] = $this->getTitle($row);

            $bookData['id'] = $this->getId($row);

            $book = Book::create($bookData);
            //Because we're not using an auto-incrementing primary key, we have to manually set the id. This could also be
            //accomplished by catching it in the creating() event listener, but this is a quick way as long as we know it's the only place we create
            //the record.
            $book->id = $bookData['id'];
            $book->save();

            $this->parseAuthors($row, $book);
        }


    }

    private function getTitle($row)
    {
        preg_match("/(?P<title>.+)(?:, by )([^,]+)/", $row, $matches, PREG_UNMATCHED_AS_NULL);

        if(count($matches) == 0) {
            preg_match("/(?P<title>.+)(?:\\s\\s)(?:[^,]+)(?:[0-9]{4})/", $row, $matches, PREG_UNMATCHED_AS_NULL);
        }
        return trim($matches['title']);
    }

    private function parseAuthors($row, $book)
    {
        preg_match("/(?:by )(?P<author>[^,]+)(?:[0-9]{4})/", $row, $matches, PREG_UNMATCHED_AS_NULL);
        if(count($matches) == 0) {
            preg_match("/(?:by )(?P<author>[^,]+)\\r\\n/", $row, $matches, PREG_UNMATCHED_AS_NULL);
        }
        if(count($matches) > 0) {
            $authors = explode("&", $matches['author']);

            $this->createAuthors($authors, $book);
        }
    }

    private function createAuthors($authors, $book)
    {
        foreach($authors as $author) {
            $author = Author::firstOrCreate(['name' => trim($author)]);

            $author->books()->attach($book);
            dump($author);
        }
    }

    private function getId($row)
    {
        preg_match("/(?P<etextid>\\s\\s[0-9]{4})/", $row, $matches, PREG_UNMATCHED_AS_NULL);
        return trim($matches['etextid']);
    }
}
