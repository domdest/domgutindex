# Dom's Gutenberg Index

This application allows the user to populate a database with the contents of one of the GUTINDEX files by year, and then to search against that database.

Dom has long been passionate about Project Gutenberg, and has in the past been a proofreader and editor in the PGDP (Project Gutenberg Distributed Proofreaders)
community. By default, this application populates the index for the year 2002, when Dom made significant contributions to the project, proofreading and editing
many texts, such as "Childe Harold's Pilgrimage", by Lord Byron.

## Developer's guide

There are a few things to be aware of for the developer in maintaining this application.

### Running the application

This application should be runnable provided Docker and Docker Compose dependencies are installed. The containers are started using a makefile. 
You can start the containers locally by running `make dev-up` and stop them by running `make dev-down`.

### Routes

The routes defined for the application can be found in `files/app/domgutindex/routes/api.php`

### Configuring and populating the index

To configure and populate the index, go to `files/app/domgutindex/config/gutindex_file.php` and change the `path` value to a local file path or a GUTINDEX URL.

Then, attach to the `php-fpm` container in your Docker Compose cluster, navigate to `/var/www/domgutindex`, and run `php artisan migrate:fresh --seed`
to rebuild the database and seed it with the contents of the index.

### Testing endpoints

The endpoints can be tested using the provided HTTP files at `files/app/domgutindex/docs`, documentation is available [here](files/app/domgutindex/docs/README.md)

### Database population

The database is populated using a seeder class, found at `files/app/domgutindex/database/seeds`. It searches the index file for relevant values using RegEx.